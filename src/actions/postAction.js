import axios from "axios";
import {
  POST_CREATE_FAIL,
  POST_CREATE_REQUEST,
  POST_CREATE_SUCCESS,
  POST_DETAILS_FAIL,
  POST_DETAILS_REQUEST,
  POST_DETAILS_SUCCESS,
  POST_EDIT_FAIL,
  POST_EDIT_REQUEST,
  POST_EDIT_SUCCESS,
  POST_EDIT_RESET,
  POST_LIST_FAIL,
  POST_LIST_REQUEST,
  POST_LIST_SUCCESS,
  POST_REMOVE_REQUEST,
  POST_REMOVE_SUCCESS,
  POST_REMOVE_FAIL,
  POST_REMOVE_RESET,
} from "../constants";

export const listPosts = () => async (dispatch) => {
  dispatch({
    type: POST_LIST_REQUEST,
  });

  try {
    const { data } = await axios.get("/posts");
    dispatch({
      type: POST_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: POST_LIST_FAIL,
      payload: error.message,
    });
  }
};

export const createPosts = (post) => async (dispatch) => {
  dispatch({
    type: POST_CREATE_REQUEST,
  });

  try {
    const { data } = await axios.post("/posts", post);

    dispatch({
      type: POST_CREATE_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: POST_CREATE_FAIL,
      payload: error.message,
    });
  }
};
export const editPost = (post) => async (dispatch) => {
  dispatch({
    type: POST_EDIT_REQUEST,
  });

  try {
    const { data } = await axios.put(`/posts/${post.id}`);

    dispatch({
      type: POST_EDIT_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: POST_EDIT_FAIL,
      payload: error.message,
    });
  }
};

export const detailsPost = (postId) => async (dispatch) => {
  dispatch({
    type: POST_DETAILS_REQUEST,
  });
  try {
    const { data } = await axios.get(`/posts/${postId}`);
    dispatch({ type: POST_DETAILS_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: POST_DETAILS_FAIL,
      payload: error.message,
    });
  }
};
export const removePost = (postId) => async (dispatch) => {
  dispatch({
    type: POST_REMOVE_REQUEST,
  });
  try {
    const { data } = await axios.delete(`/posts/${postId}`);
    dispatch({ type: POST_REMOVE_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: POST_REMOVE_FAIL,
      payload: error.message,
    });
  }
};
export const resetPost = () => async (dispatch) => {
  dispatch({
    type: POST_EDIT_RESET,
  });
  dispatch({
    type: POST_REMOVE_RESET,
  });
};
