import { combineReducers } from "redux";
import {
  postListReducer,
  postCreateReducer,
  postDetailReducer,
  postEditReducer,
  postRemoveReducer,
} from "./postReducer";

import { reducer as reduxFormReducer } from "redux-form";
import { uiReducer } from "./snackbarReducer";

export default combineReducers({
  postList: postListReducer,
  postDetail: postDetailReducer,
  postEdit: postEditReducer,
  postRemove: postRemoveReducer,
  post: postCreateReducer,
  ui: uiReducer,
  form: reduxFormReducer,
});
