import {
  POST_CREATE_FAIL,
  POST_CREATE_REQUEST,
  POST_CREATE_SUCCESS,
  POST_DETAILS_FAIL,
  POST_DETAILS_REQUEST,
  POST_DETAILS_SUCCESS,
  POST_EDIT_FAIL,
  POST_EDIT_REQUEST,
  POST_EDIT_SUCCESS,
  POST_LIST_FAIL,
  POST_LIST_REQUEST,
  POST_LIST_SUCCESS,
  POST_EDIT_RESET,
  POST_REMOVE_REQUEST,
  POST_REMOVE_SUCCESS,
  POST_REMOVE_FAIL,
  POST_REMOVE_RESET,
} from "../constants";

const initialState = {};

export const postListReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_LIST_REQUEST:
      return { loading: true, showAlert: false };
    case POST_LIST_SUCCESS:
      return { loading: false, posts: action.payload };
    case POST_LIST_FAIL:
      return { loading: false, error: action.payload, showAlert: true };
    default:
      return state;
  }
};
export const postCreateReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_CREATE_REQUEST:
      return { loading: true, show: false, success: false };
    case POST_CREATE_SUCCESS:
      return { loading: false, post: action.payload, success: true };
    case POST_CREATE_FAIL:
      return { loading: false, error: action.payload, show: true };
    default:
      return state;
  }
};
export const postDetailReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_DETAILS_REQUEST:
      return { loading: true, show: false };
    case POST_DETAILS_SUCCESS:
      return { loading: false, post: action.payload };
    case POST_DETAILS_FAIL:
      return { loading: false, error: action.payload, show: true };
    default:
      return state;
  }
};
export const postEditReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_EDIT_RESET:
      return { loading: false, post: {}, showEdit: false, success: false };
    case POST_EDIT_REQUEST:
      return { loading: true, showEdit: false, success: false };
    case POST_EDIT_SUCCESS:
      return { loading: false, post: action.payload, success: true };
    case POST_EDIT_FAIL:
      return { loading: false, errorEdit: action.payload, showEdit: true };
    default:
      return state;
  }
};
export const postRemoveReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_REMOVE_RESET:
      return { loading: false, post: {}, showEdit: false, success: false };
    case POST_REMOVE_REQUEST:
      return { loading: true, showRemove: false, success: false };
    case POST_REMOVE_SUCCESS:
      return { loading: false, post: action.payload, success: true };
    case POST_REMOVE_FAIL:
      return { loading: false, errorEdit: action.payload, showRemove: true };
    default:
      return state;
  }
};
