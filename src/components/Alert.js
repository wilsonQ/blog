import { makeStyles } from "@material-ui/core/styles";
import { Alert, AlertTitle } from "@material-ui/lab";
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
    marginTop: theme.spacing(3),
  },
}));

export default function SuccessSnackbar({ severity, message }) {
  const classes = useStyles();
  function capitalize(word) {
    const lower = word.toLowerCase();
    return word.charAt(0).toUpperCase() + lower.slice(1);
  }

  return (
    <div className={classes.root}>
      <Alert severity={severity}>
        <AlertTitle>{capitalize(severity)}</AlertTitle>
        {message}
      </Alert>
    </div>
  );
}
