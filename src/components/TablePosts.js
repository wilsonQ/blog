import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Tooltip from "@material-ui/core/Tooltip";
import DetailsIcon from "@material-ui/icons/Details";
import EditIcon from "@material-ui/icons/Edit";
import PropTypes from "prop-types";
import React from "react";
import { Link } from "react-router-dom";
import RemoveDialog from "./RemoveDialog";

const styles = (theme) => ({
  root: {
    display: "flex",
    marginTop: theme.spacing.unit * 3,
    overflowX: "hide",
  },
  table: {
    minWidth: 340,
  },
  tableCell: {
    paddingRight: 30,
    paddingLeft: 5,
  },
});

function TablePosts({ classes, data }) {
  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell className={classes.tableCell}>Title Post</TableCell>
            <TableCell align="right" className={classes.tableCell}>
              Action
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((n) => {
            return (
              <TableRow key={n.id}>
                <TableCell
                  component="th"
                  scope="row"
                  className={classes.TableCell}
                >
                  {n.title}
                </TableCell>
                <TableCell align="right" className={classes.TableCell}>
                  <div className={classes.title}>
                    <RemoveDialog id={n.id} />
                    <Tooltip title="Edit">
                      <Link to={`/post/edit/${n.id}`}>
                        <IconButton aria-label="edit">
                          <EditIcon />
                        </IconButton>
                      </Link>
                    </Tooltip>
                    <Tooltip title="Show more Details">
                      <Link to={`/post/details/${n.id}`}>
                        <IconButton aria-label="details">
                          <DetailsIcon />
                        </IconButton>
                      </Link>
                    </Tooltip>
                  </div>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
}

TablePosts.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TablePosts);
