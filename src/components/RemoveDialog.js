import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import Slide from "@material-ui/core/Slide";
import Tooltip from "@material-ui/core/Tooltip";
import DeleteIcon from "@material-ui/icons/Delete";
import React, { forwardRef, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { showSuccessSnackbar } from "../actions/alertActions";
import { removePost, resetPost } from "../actions/postAction";

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function RemoveDialog({ id }) {
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  const postRemove = useSelector((state) => state.postRemove);
  const { success, showRemove, error } = postRemove;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    dispatch(removePost(id));
  };

  useEffect(() => {
    dispatch(resetPost());
    if (success) {
      setOpen(false);
      dispatch(showSuccessSnackbar("Success Remove Post"));
    }
    return () => {
      dispatch(resetPost());
    };
  }, [dispatch, success]);

  return (
    <>
      <Tooltip title="Delete">
        <IconButton onClick={handleClickOpen} aria-label="delete">
          <DeleteIcon />
        </IconButton>
      </Tooltip>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          {showRemove ? error : "Are you sure to delete the post?"}
        </DialogTitle>
        {success && (
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              Loading ...
            </DialogContentText>
          </DialogContent>
        )}
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleClose} color="primary">
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
