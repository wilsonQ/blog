import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
  },
  toolbar: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  logo: {
    textDecoration: "none",
    color: "white",
  },
}));

export default function Navbar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar className="classes.toolbar">
          <Typography variant="h6" className={classes.title}>
            <Link className={classes.logo} to="/">
              BLOG
            </Link>
          </Typography>

          <Button component={Link} to="/post/create" color="inherit">
            Create Post
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}
