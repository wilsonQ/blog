import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  grid: {
    width: "100%",
  },
}));

export default function DisplayPost({ post }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container className={classes.grid} spacing={2}>
        <Grid item xs={12} sm container>
          <Grid item xs container direction="column" spacing={2}>
            <Grid item xs>
              <Typography gutterBottom variant="h4">
                {post.title}
              </Typography>
              <Typography variant="subtitle1" gutterBottom>
                {post.body}
              </Typography>
              <Typography variant="body1" color="textSecondary">
                ID: {post.id}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
