import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";

const required = (value) => (value ? undefined : "Required");
const renderTextField = ({
  input,
  label,
  meta: { touched, error },
  ...custom
}) => (
  <TextField
    error={touched && error}
    fullWidth
    label={label}
    floatingLabelText={label}
    margin="normal"
    variant="outlined"
    helperText={touched && error}
    {...input}
    {...custom}
  />
);

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },

  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: "25ch",
  },
  buttons: {
    display: "flex",
    flexDirection: "row",
  },
  buttonAction: { marginTop: 50, marginLeft: 60 },
  buttonActionEdit: { marginTop: 50 },
}));

const MaterialUiForm = (props) => {
  const classes = useStyles();
  const { edit, handleSubmit, pristine, reset, submitting } = props;
  return (
    <div className={classes.root}>
      <form className={classes.form} onSubmit={handleSubmit}>
        <div>
          <Field
            name="title"
            component={renderTextField}
            label="Title"
            validate={required}
          />
        </div>
        <div>
          <Field
            name="content"
            component={renderTextField}
            label="Content"
            validate={required}
          />
        </div>
        <div className={classes.buttons}>
          {!edit && (
            <Button
              style={{ marginTop: 50 }}
              margin="normal"
              variant="contained"
              color="primary"
              type="button"
              disabled={pristine || submitting}
              onClick={reset}
            >
              cancel
            </Button>
          )}
          <Button
            className={edit ? classes.buttonActionEdit : classes.buttonAction}
            margin="normal"
            type="submit"
            variant="contained"
            color="primary"
            disabled={pristine || submitting}
          >
            {edit ? "Edit" : "Create"} Post
          </Button>
        </div>
      </form>
    </div>
  );
};
function mapStateToProps(state, ownProps) {
  if (ownProps.initialValues) {
    return {
      initialValues: {
        id: ownProps.initialValues.id,
        title: ownProps.initialValues.title,
        content: ownProps.initialValues.body,
      },
    };
  }
}

export default connect(mapStateToProps)(
  reduxForm({
    form: "MaterialUiForm",
    enableReinitialize: true,
  })(MaterialUiForm)
);
