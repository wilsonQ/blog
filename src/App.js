import Container from "@material-ui/core/Container";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import Navbar from "./components/Navbar";
import Snackbar from "./components/Snackbar";
import Home from "./view/Home";
import PostCreateView from "./view/PostCreateView";
import PostsDetailsView from "./view/PostDetailsView";
import PostEditView from "./view/PostEditView";
function App() {
  return (
    <Router>
      <div>
        <Navbar />
        <Snackbar />
        <Container maxWidth="lg">
          <Switch>
            <Route exact path="/" component={Home}></Route>
            <Route exact path="/post/create" component={PostCreateView}></Route>
            <Route exact path="/post/edit/:id" component={PostEditView}></Route>
            <Route
              exact
              path="/post/details/:id"
              component={PostsDetailsView}
            ></Route>
          </Switch>
        </Container>
      </div>
    </Router>
  );
}

export default App;
