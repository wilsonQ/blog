import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { listPosts } from "../actions/postAction";
import TablePosts from "../components/TablePosts";
import LoadingBox from "../components/LoadingBox";
import Alert from "../components/Alert";
export default function Home() {
  const dispatch = useDispatch();
  const postList = useSelector((state) => state.postList);
  const { loading, error, posts } = postList;

  useEffect(() => {
    dispatch(listPosts());
  }, [dispatch]);

  return (
    <>
      {loading ? (
        <LoadingBox />
      ) : error ? (
        <Alert />
      ) : (
        <div>
          <h1>Posts</h1>
          <TablePosts data={posts || []} />
        </div>
      )}
    </>
  );
}
