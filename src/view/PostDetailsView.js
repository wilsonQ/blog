import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { detailsPost, resetPost } from "../actions/postAction";
import Alert from "../components/Alert";
import DisplayPost from "../components/DisplayPost";
import LoadingBox from "../components/LoadingBox";
export default function PostDetailsView(props) {
  const postId = props.match.params.id;
  const dispatch = useDispatch();
  const postDetail = useSelector((state) => state.postDetail);
  const { loading, error, post, show } = postDetail;

  useEffect(() => {
    dispatch(detailsPost(postId));
    dispatch(resetPost());

    return () => {
      dispatch(resetPost());
    };
  }, [dispatch, postId]);

  return (
    <>
      {loading ? (
        <LoadingBox />
      ) : show ? (
        <Alert message={error} severity="Error" />
      ) : (
        post && (
          <div>
            <h2>Details Post</h2>
            <DisplayPost post={post} />
          </div>
        )
      )}
    </>
  );
}
