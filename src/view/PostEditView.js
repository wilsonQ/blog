import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { showSuccessSnackbar } from "../actions/alertActions";
import { detailsPost, editPost, resetPost } from "../actions/postAction";
import Alert from "../components/Alert";
import FormPost from "../components/FormPost";
import LoadingBox from "../components/LoadingBox";

export default function PostEditView(props) {
  const history = useHistory();
  const postId = props.match.params.id;
  const dispatch = useDispatch();
  const postDetail = useSelector((state) => state.postDetail);
  const postEdit = useSelector((state) => state.postEdit);
  const { loading, error, post, show } = postDetail;
  const { showEdit, errorEdit, success } = postEdit;

  function handleSubmit(value) {
    dispatch(editPost(value));
  }

  useEffect(() => {
    dispatch(detailsPost(postId));
    dispatch(resetPost());
    if (success) {
      dispatch(showSuccessSnackbar("Success Edit Post"));
      history.push(`/`);
    }
    return () => {
      dispatch(resetPost());
    };
  }, [dispatch, history, postId, success]);

  return (
    <>
      {loading ? (
        <LoadingBox />
      ) : show ? (
        <Alert message={error} severity="Error" />
      ) : (
        <div>
          <h1>Edit Post</h1>
          {showEdit && <Alert message={errorEdit} severity="Error" />}
          <FormPost initialValues={post} onSubmit={handleSubmit} edit={true} />
        </div>
      )}
    </>
  );
}
