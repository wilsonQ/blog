import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { reset } from "redux-form";
import Alert from "../components/Alert";
import { createPosts, resetPost } from "../actions/postAction";
import FormPost from "../components/FormPost";
import { showSuccessSnackbar } from "../actions/alertActions";

export default function PostCreateView() {
  const dispatch = useDispatch();
  const post = useSelector((state) => state.post);
  const { error, show, success } = post;

  function handleSubmit(value) {
    dispatch(createPosts(value));
  }

  useEffect(() => {
    dispatch(resetPost());
    if (success) {
      dispatch(showSuccessSnackbar("Success Create Post"));
      dispatch(reset("MaterialUiForm"));
    }
    return () => {
      dispatch(resetPost());
    };
  }, [dispatch, success]);

  return (
    <div>
      <h1>Create new Post</h1>
      {show && <Alert message={error} severity="Error" />}
      <FormPost onSubmit={handleSubmit} />
    </div>
  );
}
